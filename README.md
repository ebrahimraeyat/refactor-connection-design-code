# Refactor connection design code

Refactoring of the code already developed for connection design.

- `local_modules:` python modules shared by many structures in the original project.
- `stair_tower`: example structure.
