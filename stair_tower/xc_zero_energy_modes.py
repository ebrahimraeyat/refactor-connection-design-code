# -*- coding: utf-8 -*-
''' Check if stifness matrix is singular.'''
exec(open('./xc_model.py').read())

# Graphic stuff.
oh= output_handler.OutputHandler(modelSpace)

oh.displayBlocks()
oh.displayFEMesh()
oh.displayLocalAxes()

# Zero energy modes.
numEigenModes= 12
analOk= modelSpace.zeroEnergyModes(numEigenModes)
computedModes= modelSpace.preprocessor.getDomain.numModes
for i in range(1,computedModes):
    oh.displayEigenvectors(mode= i)

