# -*- coding: utf-8 -*-
''' Estimate some steel quantities.'''

from __future__ import division
from __future__ import print_function


import sys
sys.path.insert(0, '../local_modules')
import bill_of_quantities as boq

exec(open('./xc_model.py').read())

        
xcTotalSet= modelSpace.preprocessor.getSets['total']        
estimator= boq.Estimator(FEcase.title)
estimator.writeCSV(xcTotalSet, '../stair_tower_boq.csv')
