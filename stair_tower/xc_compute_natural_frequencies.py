# -*- coding: utf-8 -*-
''' Compute the natural frequencies of the structure.'''

from __future__ import division
from __future__ import print_function

from solution import predefined_solutions

exec(open('./xc_model.py').read())
# Loads
loadCaseManager= lcm.LoadCaseManager(preprocessor)
loadCaseNames= ['hLoadX','hLoadY']
loadCaseManager.defineSimpleLoadCases(loadCaseNames)

## Set non-linear analysis.
modelSpace.analysis= predefined_solutions.penalty_newton_raphson(modelSpace.preprocessor.getProblem)

## Horizontal x load (to estimate structure stiffness).
cLC= loadCaseManager.setCurrentLoadCase('hLoadX')

### Horizontal load in structure
massFactor= 1.0
hLoadXVector= xc.Vector([-massFactor,0.0,0.00])
modelSpace.createSelfWeightLoad(allMemberSet,hLoadXVector)


# Natural frequency x-direction.

modelSpace.removeAllLoadPatternsFromDomain()
modelSpace.addLoadCaseToDomain('hLoadX')
result= modelSpace.analyze(calculateNodalReactions= True)

## Max displacement.
topPoint= modelSpace.getNearestPoint(geom.Pos3d(0.0,0.0,31.56))
uX= topPoint.getNode().getDisp[0]

## Total force.
Rx= 0.0
for p in supportSet.getPoints:
    n= p.getNode()
    Rx-= n.getReaction[0]

## Mass.
M= Rx/massFactor
    
## x-stiffness.
Kx= Rx/uX

## natural frequency
wx= math.sqrt(Kx/M)
Tx= 2.0*math.pi/wx
fx= 1/Tx

## Horizontal y load (to estimate structure stiffness).
cLC= loadCaseManager.setCurrentLoadCase('hLoadY')

### Horizontal load in structure
massFactor= 1.0
hLoadYVector= xc.Vector([0.0,-massFactor,0.0])
modelSpace.createSelfWeightLoad(allMemberSet,hLoadYVector)

# Natural frequency y-direction.

modelSpace.removeAllLoadPatternsFromDomain()
modelSpace.addLoadCaseToDomain('hLoadY')
result= modelSpace.analyze(calculateNodalReactions= True)

## Max displacement.
uY= topPoint.getNode().getDisp[1]

## Total force.
Ry= 0.0
for p in supportSet.getPoints:
    n= p.getNode()
    Ry-= n.getReaction[1]

## Mass.
M= Ry/massFactor
    
## y-stiffness.
Ky= Ry/uY

## natural frequency
wy= math.sqrt(Ky/M)
Ty= 2.0*math.pi/wy
fy= 1/Ty

def designResponseSpectrum(T):
    retval= 9.81 # g (m/s2)
    if(T<0.12):
        retval*= 0.0907*(0.4+0.6*T/0.12)
    elif(T<0.6):
        retval*= 0.0907
    else:
        retval*= 0.0544/T
    return retval

quakeAccelX= designResponseSpectrum(Tx)
quakeAccelY= designResponseSpectrum(Ty)

print('uX= ', uX*1e3, 'mm')
print('Rx= ', Rx/1e3, 'kN')
print('M= ', M, 'kg')
print('Kx= ', Kx/1e3, 'kN/m')
print('w_x= ', wx, 'rad/s')
print('T_x= ', Tx, 's')
print('f_x= ', fx, 'Hz')
print('quakeAccelX= ', quakeAccelX,'m/s2')

print('uY= ', uY*1e3, 'mm')
print('Ry= ', Ry/1e3, 'kN')
print('M= ', M, 'kg')
print('Ky= ', Ky/1e3, 'kN/m')
print('w_y= ', wy, 'rad/s')
print('T_y= ', Ty, 's')
print('f_y= ', fy, 'Hz')
print('quakeAccelY= ', quakeAccelY,'m/s2')

