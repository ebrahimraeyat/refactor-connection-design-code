# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

import math
from misc_utils import math_utils

I= 1.15 # Importance factor.
K_zt= 1.0 # topographic factor.
V= 155*1000/3600.0 # basic wind speed.

# Directionality factors.
Kd_lattice= 0.85

# Velocity exposure coefficient.
def Kz(z):
    ''' Return velocity exposure coefficient for stair tower.'''
    if(z<5):
        return .865
    else:
        return 2.01*math.pow(z/275.0,2.0/9.5)

# Gust effect factor.
Gf= 0.85 # rigid structure.

# Velocity pressure.
def qz(z, Kd):
    ''' Velocity pressure on stair tower.'''
    return 2131.848*Kz(z)*Kd

def qz_lattice(z):
    ''' Velocity pressure on stair tower lattice structure.'''
    return qz(z,Kd_lattice)

# def Cf_lattice(epsilon):
#     ''' Force coefficient for square trussed towers.'''
#     return 4.0*epsilon**2-5.9*epsilon + 4.0
# for i in range(1,100):
#     ratio= .01*float(i)
#     Cf= Cf_lattice(ratio)
#     print('ratio= '+str(ratio)+' Cf= '+str(Cf))

# Force coefficients

## Lattice structure force coefficient.

### Solidity ratio.

def getProjectedArea(xcSet):
    ''' Return the area projected by the elements of the set.'''
    xcSet.fillDownwards()
    solidArea= 0.0
    for e in xcSet.getElements:
        crossSection= e.getProp('crossSection') # steel shape
        solidArea+= crossSection.h()*e.getLength(True)
    return solidArea

def getSolidityRatio(xcSet, direction, widthOffset, heightOffset, solidArea):
    ''' Return the solidity ratios on x and y.'''
    xcSet.fillDownwards()
    bnd= xcSet.getBnd(1.0)
    if(direction=='X' or direction=='x'):
        width= bnd.getYMax-bnd.getYMin
    else:
        width= bnd.getXMax-bnd.getXMin
    Ag= (width+widthOffset)*(bnd.getZMax-bnd.getZMin+heightOffset)        
    eps= solidArea/Ag
    return eps

def forceCoefficient(eps, diagonal= False):
    ''' Force coefficient for square trussed towers
        according to figure 7.2-18 of SBC 301.

    :param eps: ratio of solid area to gross area of one 
                tower face for the segment under consideration.
    :param diagonal: true if wind along a tower diagonal.
    '''
    retval= 4.0*eps**2-5.9*eps+4.0
    if(diagonal):
        retval*= min(1+0.75*eps,1.2)
    return retval

# Design forces.
def F_lattice(z, Af, eps, diagonal= False):
    ''' Force in lattice members.

      Af: projected area normal to the wind.
    '''
    Cf_lattice= forceCoefficient(eps,diagonal)
    return qz_lattice(z)*Gf*Cf_lattice*Af


'''
print('V= '+str(V)+' m/s')
print('V^2= '+str(V*V)+' m2/s2')
print('I*K_zt*V^2= '+str(I*K_zt*V*V)+' N/m2')
print('K_z(z=5.001)= '+str(Kz(5.001)))
print('F_lattice(z=5.001,Af= 1.0)= '+str(F_lattice(5.001,1.0)))
print('h/D= '+str(ratio))
'''

