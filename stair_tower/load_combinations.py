# -*- coding: utf-8 -*-
from actions import combinations as cc
# Definition of USL and SLS

#    ***LIMIT STATE COMBINATIONS***
combContainer= cc.CombContainer()  #Container of load combinations


# COMBINATIONS OF ACTIONS FOR ULTIMATE LIMIT STATES
# DL= dead load
# H= load due to filling
# LL= live load
# W= wind load
# E= earthquaque load due to dead load

combContainer.ULS.perm.add('ULS01', '1.4*DL') #SBC 301 (Eq. 2.3.2-1)
combContainer.ULS.perm.add('ULS02a', '1.2*DL+1.2*Tup+1.6*LL') #SBC 301 (Eq. 2.3.2-2) 
combContainer.ULS.perm.add('ULS02b', '1.2*DL+1.2*Tdown+1.6*LL') #SBC 301 (Eq. 2.3.2-2) 
combContainer.ULS.perm.add('ULS03a', '1.2*DL+0.5*LL') #SBC 301 (Eq. 2.3.2-3)
combContainer.ULS.perm.add('ULS03b', '1.2*DL+0.8*WX') #SBC 301 (Eq. 2.3.2-3)
combContainer.ULS.perm.add('ULS03c', '1.2*DL+0.8*WY') #SBC 301 (Eq. 2.3.2-3)
combContainer.ULS.perm.add('ULS03d', '1.2*DL+0.8*WXneg') #SBC 301 (Eq. 2.3.2-3)
combContainer.ULS.perm.add('ULS03e', '1.2*DL+0.8*WYneg') #SBC 301 (Eq. 2.3.2-3)
combContainer.ULS.perm.add('ULS04a', '1.2*DL+1.6*WX+0.5*LL') #SBC 301 (Eq. 2.3.2-4)
combContainer.ULS.perm.add('ULS04b', '1.2*DL+1.6*WY+0.5*LL') #SBC 301 (Eq. 2.3.2-4)
combContainer.ULS.perm.add('ULS04c', '1.2*DL+1.6*WXneg+0.5*LL') #SBC 301 (Eq. 2.3.2-4)
combContainer.ULS.perm.add('ULS04d', '1.2*DL+1.6*WYneg+0.5*LL') #SBC 301 (Eq. 2.3.2-4)
combContainer.ULS.perm.add('ULS05a', '1.2*DL+1.0*EX+0.5*LL') #SBC 301 (Eq. 2.3.2-5)
combContainer.ULS.perm.add('ULS05b', '1.2*DL+1.0*EY+0.5*LL') #SBC 301 (Eq. 2.3.2-5)
combContainer.ULS.perm.add('ULS05c', '1.2*DL+1.0*EXneg+0.5*LL') #SBC 301 (Eq. 2.3.2-5)
combContainer.ULS.perm.add('ULS05d', '1.2*DL+1.0*EYneg+0.5*LL') #SBC 301 (Eq. 2.3.2-5)
combContainer.ULS.perm.add('ULS06a', '0.9*DL+1.6*WX+0.5*LL') #SBC 301 (Eq. 2.3.2-6)
combContainer.ULS.perm.add('ULS06b', '0.9*DL+1.6*WY+0.5*LL') #SBC 301 (Eq. 2.3.2-6)
combContainer.ULS.perm.add('ULS06c', '0.9*DL+1.6*WXneg+0.5*LL') #SBC 301 (Eq. 2.3.2-6)
combContainer.ULS.perm.add('ULS06d', '0.9*DL+1.6*WYneg+0.5*LL') #SBC 301 (Eq. 2.3.2-6)
combContainer.ULS.perm.add('ULS07a', '0.9*DL+1.0*EX+0.5*LL') #SBC 301 (Eq. 2.3.2-7)
combContainer.ULS.perm.add('ULS07b', '0.9*DL+1.0*EY+0.5*LL') #SBC 301 (Eq. 2.3.2-7)
combContainer.ULS.perm.add('ULS07c', '0.9*DL+1.0*EXneg+0.5*LL') #SBC 301 (Eq. 2.3.2-7)
combContainer.ULS.perm.add('ULS07d', '0.9*DL+1.0*EYneg+0.5*LL') #SBC 301 (Eq. 2.3.2-7)

# Dump combination definition into XC.
combContainer.dumpCombinations(preprocessor)




