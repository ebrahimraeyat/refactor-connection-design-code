# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

driftW= 0.7*155e-3 # drift due to wind.
H= 30.0 # m
driftLimit= H/200 # Process industry practices.
print('drift: ', driftW*1e3,' mm, = 1/', H/driftW)
print('drift limit: ', driftLimit*1e3,' mm')

