# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import wind_load

psfToNm2= 47.88026 # Pound by square foot to Newton by square meter.

# Loads
loadCaseManager= lcm.LoadCaseManager(preprocessor)
loadCaseNames= ['DL', 'LL', 'WX', 'WY', 'WXneg', 'WYneg', 'EX', 'EY', 'EXneg', 'EYneg', 'Tdown', 'Tup']
loadCaseManager.defineSimpleLoadCases(loadCaseNames) 

def applyAccelerationOnPermanentWeights(elementSet, accelVector):
    # Structure
    modelSpace.createSelfWeightLoad(elementSet,accelVector)

## Dead load.
cLC= loadCaseManager.setCurrentLoadCase('DL')

### Self weight
gravityVector= xc.Vector([0.0,0.0,9.81])
applyAccelerationOnPermanentWeights(allMemberSet, gravityVector)

### Dead load.
stairWidth= 1.30
stairDeadLoad= 30*9.81 # 30 kg/m2
deadLoadOnStringers= stairDeadLoad*stairWidth/2.0 #N/m
for e in stringerBeamSet.elements:
    e.vector3dUniformLoadGlobal(xc.Vector([0,0,-deadLoadOnStringers]))

## Live load.
cLC= loadCaseManager.setCurrentLoadCase('LL')
liveLoadReductionFactor= 0.85 # Not all the four floors loaded.
stairLiveLoad= 100.0*psfToNm2 #N/m2
liveLoadOnStringers= stairLiveLoad*stairWidth/2.0 #N/m

for e in stringerBeamSet.elements:
    e.vector3dUniformLoadGlobal(xc.Vector([0,0,-liveLoadOnStringers]))

## Wind load.
def applyXWind(windDir):
    '''Apply wind in x direction.'''
    ##### Wind on columns
    for e in columnSet.elements:
        z= e.getPosCentroid(True).z
        shape= e.getProp('crossSection')
        h= shape.get('h')
        f= wind_load.F_lattice(z,h, eps_x)*windDir
        e.vector3dUniformLoadGlobal(f)

    ##### Wind on yBeams
    for e in yBeamSet.elements:
        z= e.getPosCentroid(True).z
        shape= e.getProp('crossSection')
        h= shape.get('h')
        f= wind_load.F_lattice(z,h, eps_x)*windDir
        e.vector3dUniformLoadGlobal(f)

    ##### Wind on stairs
    for e in stringerBeamSet.elements:
        z= e.getPosCentroid(True).z
        shape= e.getProp('crossSection')
        stairWidth= 1.0 # stair width
        b= stairWidth/2.0
        jVector= e.getCoordTransf.getJVector
        proj= jVector.dot(windDir)
        f= proj*wind_load.F_lattice(z,b, eps_y)*jVector
        e.vector3dUniformLoadGlobal(f)
        
    ##### Wind on diagonals
    for e in diagonalSet.elements:
        z= e.getPosCentroid(True).z
        vI= e.getIVector3d(True)
        angle= vI.getAngle(geom.Vector3d(windDir[0],windDir[1],windDir[2]))
        if((abs(angle)>1e-4) and (abs(angle-math.pi)>1e-4)):
            shape= e.getProp('crossSection')
            h= shape.get('h')
            l= e.getLength(True) # only one element.
            f= wind_load.F_lattice(z,h, eps_x)
            halfF= 0.5*l*f*windDir
            loadVector= xc.Vector([halfF[0],halfF[1],halfF[2],0.0,0.0,0.0])
            eNodes= e.getNodes
            eNodes[0].newLoad(loadVector)
            eNodes[1].newLoad(loadVector)

def applyYWind(windDir):
    '''Apply wind in y direction.'''
    ##### Wind on columns
    for e in columnSet.elements:
        z= e.getPosCentroid(True).z
        shape= e.getProp('crossSection')
        h= shape.get('h')
        f= wind_load.F_lattice(z,h, eps_y)*windDir
        e.vector3dUniformLoadGlobal(f)

    ##### Wind on xBeams
    for e in xBeamSet.elements:
        z= e.getPosCentroid(True).z
        shape= e.getProp('crossSection')
        h= shape.get('h')
        f= wind_load.F_lattice(z,h, eps_y)*windDir
        e.vector3dUniformLoadGlobal(f)

    ##### Wind on stairs
    for e in stringerBeamSet.elements:
        z= e.getPosCentroid(True).z
        shape= e.getProp('crossSection')
        h= 1.5*shape.get('h') # handrails, etc.
        f= wind_load.F_lattice(z,h, eps_y)*windDir
        e.vector3dUniformLoadGlobal(f)
        
    ##### Wind on diagonals
    for e in diagonalSet.elements:
        z= e.getPosCentroid(True).z
        vI= e.getIVector3d(True)
        angle= vI.getAngle(geom.Vector3d(windDir[0],windDir[1],windDir[2]))
        if((abs(angle)>1e-4) and (abs(angle-math.pi)>1e-4)):
            shape= e.getProp('crossSection')
            h= shape.get('h')
            l= e.getLength(True)
            f= wind_load.F_lattice(z,h, eps_y)
            halfF= 0.5*l*f*windDir
            loadVector= xc.Vector([halfF[0],halfF[1],halfF[2],0.0,0.0,0.0])
            eNodes= e.getNodes
            eNodes[0].newLoad(loadVector)
            eNodes[1].newLoad(loadVector)

### Solidity ratios
solidArea_x= wind_load.getProjectedArea(frame1Set)
eps_x= wind_load.getSolidityRatio(frame1Set, direction= 'X', widthOffset= lowerColumnProfile.h(), heightOffset= yBeamProfile.h(), solidArea= solidArea_x)
solidArea_y= wind_load.getProjectedArea(frameBSet)
eps_y= wind_load.getSolidityRatio(frameBSet, direction= 'Y', widthOffset= lowerColumnProfile.b(), heightOffset= xBeamProfile.h(), solidArea= solidArea_y)
print('eps_x= ', eps_x)
print('eps_y= ', eps_y)

### X direction.
cLC= loadCaseManager.setCurrentLoadCase('WX')
windDir= xc.Vector([1.0,0.0,0.0])
applyXWind(windDir)

cLC= loadCaseManager.setCurrentLoadCase('WXneg')
windDir= xc.Vector([-1.0,0.0,0.0])
applyXWind(windDir)

### Y direction.
cLC= loadCaseManager.setCurrentLoadCase('WY')
windDir= xc.Vector([0.0,1.0,0.0])
applyYWind(windDir)
cLC= loadCaseManager.setCurrentLoadCase('WYneg')
windDir= xc.Vector([0.0,-1.0,0.0])
applyYWind(windDir)


# ## Earthquake load.
quakeAccelX= 0.889767 # m/s2
quakeAccelY= 0.889767 # m/s2

### X direction.
cLC= loadCaseManager.setCurrentLoadCase('EX')
accelVector= xc.Vector([quakeAccelX,0.0,0.0])
applyAccelerationOnPermanentWeights(allMemberSet,accelVector)
cLC= loadCaseManager.setCurrentLoadCase('EXneg')
accelVector= xc.Vector([-quakeAccelX,0.0,0.0])
applyAccelerationOnPermanentWeights(allMemberSet,accelVector)

### Y direction
cLC= loadCaseManager.setCurrentLoadCase('EY')
accelVector= xc.Vector([0.0,quakeAccelY,0.0])
applyAccelerationOnPermanentWeights(allMemberSet,accelVector)
cLC= loadCaseManager.setCurrentLoadCase('EYneg')
accelVector= xc.Vector([0.0,-quakeAccelY,0.0])
applyAccelerationOnPermanentWeights(allMemberSet,accelVector)

## Thermal load

def applyThermalLoad(loadCase, AT):
    ''' Apply a thermal load over all the model
        elements.'''
    # On trusses.
    alpha= 1.2e-5 # Thermal expansion coefficient of the steel
    trussSet= diagonalSet
    trussSetTags= list()
    for t in trussSet.getElements:
        trussSetTags.append(t.tag)

    trussThermalLoad= loadCase.newElementalLoad("truss_strain_load")
    trussThermalLoad.elementTags= xc.ID(trussSetTags)
    trussThermalLoad.eps1= alpha*AT
    trussThermalLoad.eps2= alpha*AT

    # On beams.
    barSetTags= list()
    for b in barSet.getElements:
        barSetTags.append(b.tag)
    barThermalLoad= loadCase.newElementalLoad("beam_strain_load")
    barThermalLoad.elementTags= xc.ID(barSetTags)
    thermalDeformation= xc.DeformationPlane(alpha*AT)
    barThermalLoad.backEndDeformationPlane= thermalDeformation
    barThermalLoad.frontEndDeformationPlane= thermalDeformation
    

### Temperature down (-30)
cLC= loadCaseManager.setCurrentLoadCase('Tdown')

applyThermalLoad(cLC, -30.0)
### Temperature up (+30)
cLC= loadCaseManager.setCurrentLoadCase('Tup')

applyThermalLoad(cLC, 30.0)

