# -*- coding: utf-8 -*-
''' Create the finite element mesh and element sets.'''

from __future__ import division
from __future__ import print_function

import math
import xc_base
import geom
import xc

from model import predefined_spaces
from model.boundary_cond import spring_bound_cond
from materials.aci import ACI_materials
from materials import typical_materials
from materials.sections import section_properties
from materials.astm_aisc import ASTM_materials
from postprocess import output_handler
from actions import load_cases as lcm
from actions import combinations as combs
from postprocess.config import default_config

FEcase= xc.FEProblem()
FEcase.title= 'Stair tower structure'
exec(open('./xc_model_blocks.py').read())

xcTotalSet= preprocessor.getSets.getSet('total')

# Problem type
preprocessor=FEcase.getPreprocessor
nodes= preprocessor.getNodeHandler
modelSpace= predefined_spaces.StructuralMechanics3D(nodes) 

# Import CAD information
columnSet= preprocessor.getSets.defSet('columnSet')
xBeamSet= preprocessor.getSets.defSet('xBeamSet')
xLowerBeamSet= preprocessor.getSets.defSet('xLowerBeamSet')
yBeamSet= preprocessor.getSets.defSet('yBeamSet')
yLowerBeamSet= preprocessor.getSets.defSet('yLowerBeamSet')
diagonalSet1= preprocessor.getSets.defSet('diagonalSet1')
diagonalSet2= preprocessor.getSets.defSet('diagonalSet2')
diagonalSetA= preprocessor.getSets.defSet('diagonalSetA')
diagonalSetB= preprocessor.getSets.defSet('diagonalSetB')
hDiagonalSet= preprocessor.getSets.defSet('hDiagonalSet')
supportSet= preprocessor.getSets.defSet('supportSet')
stairSupportsSet= preprocessor.getSets.defSet('stairSupportsSet')
stairBeamSet= preprocessor.getSets.defSet('stairBeamSet')
stringerBeamSet= preprocessor.getSets.defSet('stringerBeamSet')
frame1Set= preprocessor.getSets.defSet('frame1Set')
frame2Set= preprocessor.getSets.defSet('frame2Set')
frameASet= preprocessor.getSets.defSet('frameASet')
frameBSet= preprocessor.getSets.defSet('frameBSet')

setsFromLabels= {'xc_columns':columnSet, 'xc_diagonals_1':diagonalSet1, 'xc_diagonals_2':diagonalSet2, 'xc_diagonals_a':diagonalSetA, 'xc_diagonals_b':diagonalSetB, 'xc_hdiagonals':hDiagonalSet, 'xc_lower_beams_x':xLowerBeamSet, 'xc_beams_x':xBeamSet, 'xc_lower_beams_y':yLowerBeamSet, 'xc_beams_y':yBeamSet, 'xc_supports':supportSet, 'xc_supports_stairs':stairSupportsSet, 'xc_stair_beams':stairBeamSet, 'xc_stringer_beams':stringerBeamSet,'frame_1':frame1Set,'frame_2':frame2Set,'frame_a':frameASet,'frame_b':frameBSet}

## Bars
for l in xcTotalSet.getLines:
    if(l.hasProp('labels')):
        labels= l.getProp('labels')
        for key in setsFromLabels:
            if(labels.count(key)>0):
                xcSet= setsFromLabels[key]
                xcSet.getLines.append(l)
## Supports
for p in xcTotalSet.getPoints:
    if(p.hasProp('labels')):
        labels= p.getProp('labels')
        if(labels.count('xc_supports')>0):
            supportSet.getPoints.append(p)
        if(labels.count('xc_supports_stairs')>0):
            stairSupportsSet.getPoints.append(p)

lowerColumnSet= preprocessor.getSets.defSet('lowerColumnSet')
intermediateColumnSet= preprocessor.getSets.defSet('intermediateColumnSet')
upperColumnSet= preprocessor.getSets.defSet('upperColumnSet')
topColumnSet= preprocessor.getSets.defSet('topColumnSet')

for l in columnSet.getLines:
    z= l.getCentroid().z
    if(z<10.12):
        lowerColumnSet.getLines.append(l)
    elif(z<15.0):
        intermediateColumnSet.getLines.append(l)
    elif(z<23.55):
        upperColumnSet.getLines.append(l)
    else:
        topColumnSet.getLines.append(l)

lowerColumnSet.fillDownwards()
intermediateColumnSet.fillDownwards()
upperColumnSet.fillDownwards()
topColumnSet.fillDownwards()
            
# Materials
## Steel material
steel_W= ASTM_materials.A992   #steel support W shapes
steel_W.gammaM= 1.00
steel_HSS= ASTM_materials.A500 #steel support HSS shapes
steel_HSS.gammaM= 1.00
steel_C= ASTM_materials.A36
steel_C.gammaM= 1.00
xcSteel_HSS= steel_HSS.defElasticMaterial(preprocessor)
## Profile geometry
lowerColumnProfile= ASTM_materials.WShape(steel_W,'W18X143')
intermediateColumnProfile= ASTM_materials.WShape(steel_W,'W18X76')
upperColumnProfile= ASTM_materials.WShape(steel_W,'W18X46')
topColumnProfile= ASTM_materials.WShape(steel_W,'W18X35')

yDiagonalProfile= ASTM_materials.HSSShape(steel_HSS,'HSS4X4X1/4')
xDiagonalProfile= ASTM_materials.HSSShape(steel_HSS,'HSS4X4X1/4')
hDiagonalProfile= ASTM_materials.HSSShape(steel_HSS,'HSS3X3X5/16')

xLowerBeamProfile= ASTM_materials.WShape(steel_W,'W18X35')
xBeamProfile= ASTM_materials.WShape(steel_W,'W12X26')

yLowerBeamProfile= ASTM_materials.WShape(steel_W,'W18X119')
yBeamProfile= ASTM_materials.WShape(steel_W,'W12X35')

stringerBeamProfile= ASTM_materials.CShape(steel_C,'C10X25')

stairBeamProfile= yBeamProfile

# Mesh generation.
coordTransf= preprocessor.getTransfCooHandler.newCorotCrdTransf3d('coordTransf')
coordTransf.xzVector= xc.Vector([1.0,0,0])

seedElemHandler= preprocessor.getElementHandler.seedElemHandler
seedElemHandler.defaultTransformation= 'coordTransf'

## Columns.
modelSpace.createElasticBeams(lowerColumnSet, lowerColumnProfile, coordTransf, xc.Vector([1.0,0.0,0.0]), nDiv= 6)
modelSpace.createElasticBeams(intermediateColumnSet, intermediateColumnProfile, coordTransf, xc.Vector([1.0,0.0,0.0]), nDiv= 6)
modelSpace.createElasticBeams(upperColumnSet, upperColumnProfile, coordTransf, xc.Vector([1.0,0.0,0.0]), nDiv= 6)
modelSpace.createElasticBeams(topColumnSet, topColumnProfile, coordTransf, xc.Vector([1.0,0.0,0.0]), nDiv= 6)
## Diagonals.
### On frames
modelSpace.createTrusses(diagonalSetA, xcSteel_HSS, yDiagonalProfile.A(), crossSection= yDiagonalProfile, corotational= True)
modelSpace.createTrusses(diagonalSetB, xcSteel_HSS, yDiagonalProfile.A(), crossSection= yDiagonalProfile, corotational= True)
modelSpace.createTrusses(diagonalSet1, xcSteel_HSS, xDiagonalProfile.A(), crossSection= xDiagonalProfile, corotational= True)
modelSpace.createTrusses(diagonalSet2, xcSteel_HSS, xDiagonalProfile.A(), crossSection= xDiagonalProfile, corotational= True)
### Horizontal bracing
modelSpace.createTrusses(hDiagonalSet, xcSteel_HSS, hDiagonalProfile.A(), crossSection= hDiagonalProfile, corotational= True)

## Main beams.
modelSpace.createElasticBeams(xLowerBeamSet, xLowerBeamProfile, coordTransf, xc.Vector([0,1.0,0]), nDiv= 6)
modelSpace.createElasticBeams(xBeamSet, xBeamProfile, coordTransf, xc.Vector([0,1.0,0]), nDiv= 6)
modelSpace.createElasticBeams(yLowerBeamSet, yLowerBeamProfile, coordTransf, xc.Vector([1.0,0,0]))
modelSpace.createElasticBeams(yBeamSet, yBeamProfile, coordTransf, xc.Vector([1.0,0,0]))
## Stringer beams.
modelSpace.createElasticBeams(stringerBeamSet, stringerBeamProfile, coordTransf, xc.Vector([0,1.0,0]), nDiv= 6)

## Stair beams.
modelSpace.createElasticBeams(stairBeamSet, stairBeamProfile, coordTransf, xc.Vector([1.0,0,0]))

# Constraints
## Columns
for p in supportSet.getPoints:
    if(not p.hasNode):
        lmsg.warning('point: '+str(p)+' not meshed.')
    n= p.getNode()
    modelSpace.fixNode('000_FFF',n.tag)
## Stairs
springs= spring_bound_cond.SpringBC('springs',modelSpace,Kx= 1e6, Ky= 1e6, Kz=10e6)
stairSupportNodes= list()
for p in stairSupportsSet.getPoints:
    print(p.tag)
    if(not p.hasNode):
        lmsg.warning('point: '+str(p)+' not meshed.')
    n= p.getNode()
    #modelSpace.fixNode('000_FFF',n.tag)
    stairSupportNodes.append(n)
springs.applyOnNodesLst(stairSupportNodes)


# Set definition
diagonalSet= modelSpace.setSum('diagonalSet',[diagonalSetA, diagonalSetB, diagonalSet1, diagonalSet2, hDiagonalSet])
columnSet= modelSpace.setSum('columnSet',[lowerColumnSet, intermediateColumnSet, upperColumnSet, topColumnSet])
columnSet.fillDownwards()
barSet= modelSpace.setSum('barSet',[columnSet,xLowerBeamSet, yLowerBeamSet, xBeamSet,yBeamSet,stringerBeamSet,stairBeamSet])

# Member sets.
mainMemberSet= modelSpace.setSum('mainMemberSet',[barSet])
secondaryMemberSet= modelSpace.setSum('secondaryMemberSet',[diagonalSet])
allMemberSet= modelSpace.setSum('allMemberSet', [mainMemberSet, secondaryMemberSet])

columnSet.description= 'Columns'
xLowerBeamSet.description= 'Beams on frames A and B, levels 1 to 3'
yLowerBeamSet.description= 'Beams on frames 1 and 2, levels 1 to 3'
xBeamSet.description= 'Beams on frames A and B, levels 4 and up'
yBeamSet.description= 'Beams on frames 1 and 2, levels 4 and up'
diagonalSetA.description= 'Bracing on frame A'
diagonalSetB.description= 'Bracing on frame B'
diagonalSet1.description= 'Bracing on frame 1'
diagonalSet2.description= 'Bracing on frame 2'
hDiagonalSet.description= 'Horizontal bracing (level 4)'
stairBeamSet.description= 'Stair landing beams'
stringerBeamSet.description= 'Stringer beams'
# Graphic stuff.
oh= output_handler.OutputHandler(modelSpace)

#oh.displayBlocks()#setToDisplay= stringerBeamSet)
oh.displayFEMesh(setsToDisplay=[allMemberSet])
#oh.displayLocalAxes()
#oh.displayLoads()
#oh.displayReactions()
#oh.displayDispRot(itemToDisp='uX')
#oh.displayDispRot(itemToDisp='uY')
#oh.displayDispRot(itemToDisp='uZ')
