# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

import sys
import xc_base
import geom
import xc

from model import predefined_spaces
from materials.aci import ACI_materials
from postprocess import output_handler
from import_export import block_topology_entities as bte

inputFileName= sys.argv[1]
outputFileName= sys.argv[2]

FEcase= xc.FEProblem()
FEcase.title= 'Stair tower structure'
exec(open(inputFileName)).read())

xcTotalSet= preprocessor.getSets.getSet('total')
xcTotalSet.getEntities.splitLinesAtIntersections(1e-3)

bdTotal= bte.BlockData()
bdTotal.readFromXCSet(xcTotalSet)
bdTotal.writeDxfFile(outputFileName)

# Problem type
preprocessor=FEcase.getPreprocessor
nodes= preprocessor.getNodeHandler
modelSpace= predefined_spaces.StructuralMechanics3D(nodes) 


# Graphic stuff.
oh= output_handler.OutputHandler(modelSpace)

oh.displayBlocks()
#oh.displayLocalAxes(setToDisplay= girderSet)
#oh.displayLocalAxes(setToDisplay= lvlBlindFasciaSet)

#oh.displayFEMesh()
#oh.displayLoads()#setToDisplay= lvlBlindFasciaSet)

#oh.displayDispRot(itemToDisp='uY')
#oh.displayDispRot(itemToDisp='uZ')
# oh.displayIntForcDiag(itemToDisp= 'Mz', setToDisplay= jackTrussesSet)
# oh.displayIntForcDiag(itemToDisp= 'Vy', setToDisplay= jackTrussesSet)
# oh.displayIntForcDiag(itemToDisp= 'Mz', setToDisplay= girderSet)
# oh.displayReactions(setToDisplay= girderSet)
#oh.displayIntForcDiag(itemToDisp= 'Vy', setToDisplay= regularTrussesSet+jackTrussesSet+girderSet)
#oh.displayIntForcDiag(itemToDisp= 'Vy', setToDisplay= lvlBlindFasciaSet)
#oh.displayIntForcDiag(itemToDisp= 'Vy', setToDisplay= girderSet)
#oh.displayIntForcDiag(itemToDisp= 'Mz', setToDisplay= regularTrussesSet)
#oh.displayReactions(setToDisplay= regularTrussesSet)
#oh.displayIntForcDiag(itemToDisp= 'Vy', setToDisplay= regularTrussesSet)
#oh.displayIntForc('Q1')
