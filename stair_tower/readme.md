# Stair tower analysis model

## Structure model.

- `import_from_dxf.py`: imports the model geometry from the DXF file.
- `xc_model_blocks.py`: block topology imported from DXF file.
- `xc_model.py`: create the finite element mesh and element sets. 


## Main scripts

- `xc_zero_energy_modes.py`: check if stifness matrix is singular.
- `xc_ill_conditioning.py`: displays the eigenvectors corresponding to the lowest eigenvalues.
- `xc_analyze.py`: display results for each load case.
- `xc_bill_fo_quantities.py`: estimates some steel quantities.
- `xc_compute_natural_frequencies.py`: compute the natural frequencies of the structure.
- `xc_run_uls_checking.py` : check the ultimate limit states of the structure.
- `xc_write_reports.py`: write latex reports about analysis results.
- `xc_display_uls_checking.py`: show the results of the ULS checking.

## Connection scripts

- `xc_rough_diagonal_fastener_design.py`: make a rough check of a diagonal fastener design defined by the user. 
- `xc_diagonal_baseplate_connections.py`: create the baseplate connections geometry.
- `base_plates/xc_base_plate_design.py`: base plate preliminary design.
- `base_plates/xc_connection_model.py`: base plate connection finite element model.
- `base_plates/verif_vonmises.py`: check base plate Von Mises stresses
- `base_plates/display_vonmises_verif.py`: display checking results.

