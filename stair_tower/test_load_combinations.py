# -*- coding: utf-8 -*-
from actions import combinations as cc
# Definition of USL and SLS

#    ***LIMIT STATE COMBINATIONS***
combContainer= cc.CombContainer()  #Container of load combinations


# COMBINATIONS OF ACTIONS FOR ULTIMATE LIMIT STATES
# DL= dead load
# H= load due to filling
# LL= live load
# W= wind load
# E= earthquaque load due to dead load

combContainer.ULS.perm.add('prb', '1.2*DL+1.6*WX+0.5*LL') #SBC 301 (Eq. 2.3.2-4)
combContainer.ULS.perm.add('ULS04a', '1.2*DL+1.6*WX+0.5*LL') #SBC 301 (Eq. 2.3.2-4)
combContainer.ULS.perm.add('ULS06a', '0.9*DL+1.6*WX+0.5*LL') #SBC 301 (Eq. 2.3.2-6)


combContainer.dumpCombinations(preprocessor)




